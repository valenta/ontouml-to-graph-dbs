<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="OntoUML&#xa;model&#xa;instantiation" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1090958577" CREATED="1409300609620" MODIFIED="1672761763707"><hook NAME="MapStyle" background="#2e3440" zoom="2.2155797">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false" associatedTemplateLocation="template:/dark_nord_template.mm"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_671184412" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#484747" BACKGROUND_COLOR="#eceff4" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#88c0d0" WIDTH="2" TRANSPARENCY="255" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_671184412" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#81a1c1" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" BORDER_WIDTH="1.9 px">
<edge STYLE="bezier" COLOR="#81a1c1" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ebcb8b">
<icon BUILTIN="clock2"/>
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating" COLOR="#484747">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#e5e9f0" BACKGROUND_COLOR="#5e81ac" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#5e81ac"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_779275544" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#bf616a">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#bf616a" TRANSPARENCY="255" DESTINATION="ID_779275544"/>
<font SIZE="14"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#484747" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font NAME="Ubuntu" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#eceff4" BACKGROUND_COLOR="#d08770" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt">
<font NAME="Ubuntu" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#3b4252" BACKGROUND_COLOR="#ebcb8b">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#2e3440" BACKGROUND_COLOR="#a3be8c">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#2e3440" BACKGROUND_COLOR="#b48ead">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BACKGROUND_COLOR="#81a1c1">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#88c0d0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#8fbcbb">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#d8dee9">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BACKGROUND_COLOR="#e5e9f0">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BACKGROUND_COLOR="#eceff4">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font BOLD="true"/>
<node TEXT="What?" POSITION="right" ID="ID_1799186892" CREATED="1672755544778" MODIFIED="1672755551694">
<node TEXT="Individual&#xa;OntoUML&#xa;Constructs" ID="ID_1860631423" CREATED="1672755592681" MODIFIED="1672755618503">
<node TEXT="" ID="ID_1829309949" CREATED="1672762386703" MODIFIED="1672762386703">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Kinds&#xa;and&#xa;Subkinds" ID="ID_979014521" CREATED="1672755625136" MODIFIED="1672755653759"/>
<node TEXT="Phases" ID="ID_1268240652" CREATED="1672761780383" MODIFIED="1672761786372"/>
<node TEXT="Relations" ID="ID_931304469" CREATED="1672761787551" MODIFIED="1672761790835"/>
<node TEXT="..." ID="ID_1491171025" CREATED="1672761791407" MODIFIED="1672761792667"/>
<node TEXT="" ID="ID_211037641" CREATED="1672762386700" MODIFIED="1672762386702">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="" ID="ID_230643107" CREATED="1672762386707" MODIFIED="1672762955543">
<arrowlink DESTINATION="ID_1267007228" MIDDLE_LABEL="are implemented by" STARTINCLINATION="6.75 pt;-54 pt;" ENDINCLINATION="194.24999 pt;0 pt;"/>
</node>
</node>
</node>
</node>
<node TEXT="How?" POSITION="right" ID="ID_1442539884" CREATED="1672755552297" MODIFIED="1672755559430">
<node TEXT="Using&#xa;Databases" ID="ID_1486896003" CREATED="1672755676369" MODIFIED="1672755691846">
<node TEXT="RDBMS" ID="ID_318579993" CREATED="1672755696681" MODIFIED="1672755708059"/>
<node TEXT="ODBMS" ID="ID_546374992" CREATED="1672755708569" MODIFIED="1672755710829"/>
<node TEXT="GDBMS" ID="ID_1755892317" CREATED="1672755711328" MODIFIED="1672755718061">
<node TEXT="Labeled" ID="ID_291055849" CREATED="1672756134800" MODIFIED="1672756139445">
<node TEXT="Neo4j" ID="ID_1256077366" CREATED="1672756155760" MODIFIED="1672756165478">
<node TEXT="" ID="ID_708104118" CREATED="1672762617689" MODIFIED="1672762617689">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="multi-labeled model" ID="ID_711289975" CREATED="1672761832615" MODIFIED="1672761846708"/>
<node TEXT="triggers" ID="ID_1852881989" CREATED="1672761847739" MODIFIED="1672761850947"/>
<node TEXT="" ID="ID_793727263" CREATED="1672762617687" MODIFIED="1672762617688">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="" ID="ID_1267007228" CREATED="1672762617690" MODIFIED="1672762617690"/>
</node>
</node>
</node>
<node TEXT="Typed" ID="ID_1018181726" CREATED="1672756139968" MODIFIED="1672756142547"/>
</node>
</node>
</node>
<node TEXT="Why?" POSITION="right" ID="ID_1135323591" CREATED="1672755559946" MODIFIED="1672755562789">
<node TEXT="IS databases" ID="ID_930722244" CREATED="1672755757385" MODIFIED="1672755909597"/>
<node TEXT="Model verification" ID="ID_1310372140" CREATED="1672755910969" MODIFIED="1672755938741">
<node TEXT="domain experts" ID="ID_1134465344" CREATED="1672762588789" MODIFIED="1672763034906">
<arrowlink DESTINATION="ID_1267007228" MIDDLE_LABEL="used to verify domain model" STARTINCLINATION="14.25 pt;68.25 pt;" ENDINCLINATION="187.49999 pt;0 pt;"/>
</node>
</node>
</node>
</node>
</map>
