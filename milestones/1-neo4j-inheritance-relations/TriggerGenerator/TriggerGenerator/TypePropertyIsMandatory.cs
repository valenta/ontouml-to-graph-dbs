﻿namespace TriggerGenerator
{
    internal class TypePropertyIsMandatory : TextPattern
    {
        public string TypeName { get; set; }

        public string PropertyName { get; set; }

        public TypePropertyIsMandatory(string typeName, string propertyName)
        {
            TypeName = typeName;
            PropertyName = propertyName;
        }

        public override string GenerateText() => $@"//{TypeName.ToUpper()}-{PropertyName.ToUpper()}
CALL apoc.trigger.add(""{TypeName.ToLower()}_{PropertyName.ToLower()}_mandatory"",
""UNWIND ($createdNodes + apoc.trigger.nodesByLabel($assignedLabels, '{TypeName.ToUpper()}') + apoc.trigger.nodesByLabel($removedNodeProperties, '{TypeName.ToUpper()}')) AS node
CALL apoc.util.validate(apoc.label.exists(node, '{TypeName.ToUpper()}') and not exists(node.{PropertyName}), '{PropertyName} property of {TypeName} is mandatory', null)
RETURN null"", {{phase:'before'}});
";
    }
}
