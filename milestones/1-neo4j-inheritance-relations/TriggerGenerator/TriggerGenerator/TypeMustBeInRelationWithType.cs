﻿namespace TriggerGenerator
{
    internal class TypeMustBeInRelationWithType : TextPattern
    {
        public string TypeMandatory { get; set; }

        public string TypeRelated { get; set; }

        public TypeMustBeInRelationWithType(string type1, string type2)
        {
            TypeMandatory = type1;
            TypeRelated = type2;
        }

        public override string GenerateText() => $@"//{TypeMandatory.ToUpper()}-{TypeRelated.ToUpper()}
CALL apoc.trigger.add(""{TypeMandatory.ToLower()}_must_be_in_relation_with_{TypeRelated.ToLower()}_assign"",
""UNWIND apoc.trigger.nodesByLabel($assignedLabels, '{TypeMandatory.ToUpper()}') AS node1
CALL apoc.util.validate(SIZE([(node1)--(n2:{TypeRelated.ToUpper()}) | n2 ]) < 1, '{TypeMandatory} instance must be in relation with {TypeRelated} instance', null)
RETURN null"", {{phase:'before'}});
CALL apoc.trigger.add(""{TypeMandatory.ToLower()}_must_be_in_relation_with_{TypeRelated.ToLower()}_remove"",
""UNWIND apoc.trigger.nodesByLabel($removedLabels, '{TypeRelated.ToUpper()}') AS node2
UNWIND [(n1:{TypeMandatory.ToUpper()})--(node2) | n1 ] AS node1
CALL apoc.util.validate(SIZE([(node1)--(n2:{TypeRelated.ToUpper()}) | n2 ]) < 1, '{TypeMandatory} instance must be in relation with {TypeRelated} instance', null)
RETURN null"", {{phase:'before'}});
CALL apoc.trigger.add(""{TypeMandatory.ToLower()}_must_be_in_relation_with_{TypeRelated.ToLower()}_delete"",
""UNWIND $deletedRelationships AS r
UNWIND [apoc.rel.startNode(r), apoc.rel.endNode(r)] AS node1
CALL apoc.util.validate(apoc.label.exists(node1, '{TypeMandatory.ToUpper()}') and (SIZE([(node1)--(n2:{TypeRelated.ToUpper()}) | n2 ]) < 1), '{TypeMandatory} instance must be in relation with {TypeRelated} instance', null)
RETURN null"", {{phase:'before'}});
";
    }
}
