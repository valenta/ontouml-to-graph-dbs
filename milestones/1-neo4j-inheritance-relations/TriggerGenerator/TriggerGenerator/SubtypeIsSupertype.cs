﻿namespace TriggerGenerator
{
    internal class SubtypeIsSupertype : TextPattern
    {
        public string Subtype { get; set; }

        public string Supertype { get; set; }

        public SubtypeIsSupertype(string subtype, string supertype)
        {
            Subtype = subtype;
            Supertype = supertype;
        }

        public override string GenerateText() => $@"//{Subtype.ToUpper()}-{Supertype.ToUpper()}
CALL apoc.trigger.add(""{Subtype.ToLower()}_is_{Supertype.ToLower()}"",
""UNWIND (apoc.trigger.nodesByLabel($assignedLabels, '{Subtype.ToUpper()}') + apoc.trigger.nodesByLabel($removedLabels, '{Supertype.ToUpper()}')) AS node
CALL apoc.util.validate(apoc.label.exists(node, '{Subtype.ToUpper()}') and not apoc.label.exists(node, '{Supertype.ToUpper()}'), '{Subtype} instance must be {Supertype} instance', null)
RETURN null"", {{phase:'before'}});
";
    }
}
