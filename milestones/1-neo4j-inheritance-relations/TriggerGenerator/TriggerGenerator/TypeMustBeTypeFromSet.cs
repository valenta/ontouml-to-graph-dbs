﻿namespace TriggerGenerator
{
    internal class TypeMustBeTypeFromSet : TextPattern
    {
        public string Type { get; set; }

        public List<string> TypeSet { get; set; }

        public TypeMustBeTypeFromSet(string type, List<string> typeSet)
        {
            Type = type;
            TypeSet = typeSet;
        }

        public override string GenerateText()
        {
            List<string> typeSetUpper = TypeSet.Select(x => x.ToUpper()).ToList();
            List<string> typeSetLower = TypeSet.Select(x => x.ToLower()).ToList();

            List<string> typeSetNodesByLabel = typeSetUpper.Select(s => $"apoc.trigger.nodesByLabel($removedLabels, '{s}')").ToList();
            List<string> typeSetLabelExists = typeSetUpper.Select(s => $"apoc.label.exists(node, '{s}')").ToList();

            string commaSet = typeSetUpper.Aggregate((acc, s) => $"{acc}, {s}");
            string orSet = TypeSet.Aggregate((acc, s) => $"{acc} or {s}");
            string orLowerSet = typeSetLower.Aggregate((acc, s) => $"{acc}_or_{s}");
            string nodesByLabelSet = typeSetNodesByLabel.Aggregate((acc, s) => $"{acc} + {s}");
            string labelExistsSet = typeSetLabelExists.Aggregate((acc, s) => $"{acc} or {s}");

            return $@"//{Type.ToUpper()}-SET{{{commaSet}}}
CALL apoc.trigger.add(""{Type.ToLower()}_is_{orLowerSet}"",
""UNWIND (apoc.trigger.nodesByLabel($assignedLabels, '{Type.ToUpper()}') + {nodesByLabelSet}) AS node
CALL apoc.util.validate(apoc.label.exists(node, '{Type.ToUpper()}') and not ({labelExistsSet}), '{Type} instance must be {orSet} instance', null)
RETURN null"", {{phase:'before'}});
";
        }
    }
}
