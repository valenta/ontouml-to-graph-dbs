﻿namespace TriggerGenerator
{
    internal class TypePropertyMustBeDatatype : TextPattern
    {
        public string TypeName { get; set; }

        public string PropertyName { get; set; }

        public string DatatypeName { get; set; }

        public TypePropertyMustBeDatatype(string typeName, string propertyName, string datatypeName)
        {
            TypeName = typeName;
            PropertyName = propertyName;
            DatatypeName = datatypeName;
        }

        public override string GenerateText() => $@"//{TypeName.ToUpper()}-{PropertyName.ToUpper()}-{DatatypeName.ToUpper()}
CALL apoc.trigger.add(""{TypeName.ToLower()}_{PropertyName.ToLower()}_must_be_datatype_{DatatypeName.ToLower()}"",
""UNWIND (apoc.trigger.nodesByLabel($assignedLabels, '{TypeName.ToUpper()}') + apoc.trigger.nodesByLabel($assignedNodeProperties, '{TypeName.ToUpper()}')) AS node
CALL apoc.util.validate(exists(node.{PropertyName}) and not apoc.meta.cypher.isType(node.{PropertyName}, '{DatatypeName.ToUpper()}'), '{PropertyName} property of {TypeName} must be datatype {DatatypeName}', null)
RETURN null"", {{phase:'before'}});
";
    }
}
