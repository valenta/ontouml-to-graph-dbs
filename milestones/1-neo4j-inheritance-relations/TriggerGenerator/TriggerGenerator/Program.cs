﻿namespace TriggerGenerator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string output = @"triggers.txt";

            List<TextPattern> patterns = new List<TextPattern>();

            //1-kinds
            /*patterns.Add(new TypePropertyMustBeDatatype("Battery", "id", "INTEGER"));
            patterns.Add(new TypePropertyMustBeDatatype("Battery", "maximalVoltage", "FLOAT"));
            patterns.Add(new TypePropertyMustBeDatatype("Battery", "minimalVoltage", "FLOAT"));

            patterns.Add(new TypePropertyMustBeDatatype("ElectricalDevice", "id", "INTEGER"));
            patterns.Add(new TypePropertyMustBeDatatype("ElectricalDevice", "nominalVoltage", "FLOAT"));
            patterns.Add(new TypePropertyMustBeDatatype("ElectricalDevice", "nominalCurrent", "FLOAT"));

            patterns.Add(new TypePropertyMustBeDatatype("DCPowerSupply", "id", "INTEGER"));
            patterns.Add(new TypePropertyMustBeDatatype("DCPowerSupply", "inputVoltage", "FLOAT"));
            patterns.Add(new TypePropertyMustBeDatatype("DCPowerSupply", "maximalCurrent", "FLOAT"));

            patterns.Add(new TypePropertyIsUnique("Battery", "id"));
            patterns.Add(new TypePropertyIsUnique("ElectricalDevice", "id"));
            patterns.Add(new TypePropertyIsUnique("DCPowerSupply", "id"));

            patterns.Add(new TypePropertyIsMandatory("Battery", "id"));
            patterns.Add(new TypePropertyIsMandatory("Battery", "maximalVoltage"));
            patterns.Add(new TypePropertyIsMandatory("Battery", "minimalVoltage"));

            patterns.Add(new TypePropertyIsMandatory("ElectricalDevice", "id"));
            patterns.Add(new TypePropertyIsMandatory("ElectricalDevice", "nominalVoltage"));
            patterns.Add(new TypePropertyIsMandatory("ElectricalDevice", "nominalCurrent"));

            patterns.Add(new TypePropertyIsMandatory("DCPowerSupply", "id"));
            patterns.Add(new TypePropertyIsMandatory("DCPowerSupply", "inputVoltage"));
            patterns.Add(new TypePropertyIsMandatory("DCPowerSupply", "maximalCurrent"));

            patterns.Add(new TypeCannotBeTypeFromSet("Battery", new List<string>() { "ElectricalDevice", "DCPowerSupply", "ElectricalConnection" }));
            patterns.Add(new TypeCannotBeTypeFromSet("ElectricalDevice", new List<string>() { "Battery", "DCPowerSupply", "ElectricalConnection" }));
            patterns.Add(new TypeCannotBeTypeFromSet("DCPowerSupply", new List<string>() { "ElectricalDevice", "Battery", "ElectricalConnection" }));
            patterns.Add(new TypeCannotBeTypeFromSet("ElectricalConnection", new List<string>() { "ElectricalDevice", "DCPowerSupply", "Battery" }));*/

            //2-kind-subkinds
            /*patterns.Add(new TypePropertyMustBeDatatype("Rechargeable", "cyclelife", "INTEGER"));
            patterns.Add(new TypePropertyMustBeDatatype("Nonrechargeable", "shelflife", "INTEGER"));

            patterns.Add(new TypePropertyIsMandatory("Rechargeable", "cyclelife"));
            patterns.Add(new TypePropertyIsMandatory("Nonrechargeable", "shelflife"));

            patterns.Add(new SubtypeIsSupertype("Rechargeable", "Battery"));
            patterns.Add(new SubtypeIsSupertype("Nonrechargeable", "Battery"));

            patterns.Add(new TypeCannotBeTypeFromSet("Rechargeable", new List<string>() { "Nonrechargeable" }));
            patterns.Add(new TypeCannotBeTypeFromSet("Nonrechargeable", new List<string>() { "Rechargeable" }));

            patterns.Add(new TypeMustBeTypeFromSet("Battery", new List<string>() { "Rechargeable", "Nonrechargeable" }));*/

            //3-categories-kinds
            /*patterns.Add(new TypePropertyMustBeDatatype("EnergyStorage", "capacity", "FLOAT"));
            patterns.Add(new TypePropertyMustBeDatatype("DCVoltageSource", "actualVoltage", "FLOAT"));
            patterns.Add(new TypePropertyMustBeDatatype("DCVoltageSource", "nominalVoltage", "FLOAT"));

            patterns.Add(new TypePropertyIsMandatory("EnergyStorage", "capacity"));
            patterns.Add(new TypePropertyIsMandatory("DCVoltageSource", "actualVoltage"));
            patterns.Add(new TypePropertyIsMandatory("DCVoltageSource", "nominalVoltage"));

            patterns.Add(new TypeMustBeTypeFromSet("EnergyStorage", new List<string>() { "Battery" }));
            patterns.Add(new TypeMustBeTypeFromSet("DCVoltageSource", new List<string>() { "Battery", "DCPowerSupply" }));

            patterns.Add(new SubtypeIsSupertype("Battery", "EnergyStorage"));
            patterns.Add(new SubtypeIsSupertype("Battery", "DCVoltageSource"));
            patterns.Add(new SubtypeIsSupertype("DCPowerSupply", "DCVoltageSource"));

            patterns.Add(new TypeCannotBeTypeFromSet("Baterry", new List<string>() { "DCPowerSupply" }));
            patterns.Add(new TypeCannotBeTypeFromSet("DCPowerSupply", new List<string>() { "Baterry" }));*/

            //4-kind-phases
            /*patterns.Add(new TypePropertyMustBeDatatype("Charged", "chargeDate", "DATE"));
            patterns.Add(new TypePropertyMustBeDatatype("Used", "lastUseDate", "DATE"));
            patterns.Add(new TypePropertyMustBeDatatype("Discharged", "dischargeDate", "DATE"));

            /*patterns.Add(new TypePropertyIsMandatory("Charged", "chargeDate"));
            patterns.Add(new TypePropertyIsMandatory("Used", "lastUseDate"));
            patterns.Add(new TypePropertyIsMandatory("Discharged", "dischargeDate"));

            patterns.Add(new SubtypeIsSupertype("Charged", "Battery"));
            patterns.Add(new SubtypeIsSupertype("Used", "Battery"));
            patterns.Add(new SubtypeIsSupertype("Discharged", "Battery"));

            patterns.Add(new TypeCannotBeTypeFromSet("Charged", new List<string>() { "Used", "Discharged" }));
            patterns.Add(new TypeCannotBeTypeFromSet("Used", new List<string>() { "Charged", "Discharged" }));
            patterns.Add(new TypeCannotBeTypeFromSet("Discharged", new List<string>() { "Charged", "Used" }));

            patterns.Add(new TypeMustBeTypeFromSet("Battery", new List<string>() { "Charged", "Used", "Discharged" }));*/

            //5-kinds-roles-rolemixin-relator
            /*patterns.Add(new TypePropertyMustBeDatatype("ElectricalConnection", "id", "INTEGER"));
            patterns.Add(new TypePropertyMustBeDatatype("ElectricalConnection", "resistance", "FLOAT"));
            patterns.Add(new TypePropertyMustBeDatatype("ConnectedDCVoltageSource", "drawnCurrent", "FLOAT"));

            patterns.Add(new TypePropertyIsUnique("ElectricalConnection", "id"));

            patterns.Add(new TypePropertyIsMandatory("ElectricalConnection", "id"));
            patterns.Add(new TypePropertyIsMandatory("ElectricalConnection", "resistance"));
            patterns.Add(new TypePropertyIsMandatory("ConnectedDCVoltageSource", "drawnCurrent"));

            patterns.Add(new TypeMustBeTypeFromSet("ConnectedDCVoltageSource", new List<string>() { "ConnectedBattery", "ConnectedDCPowerSupply" }));

            patterns.Add(new SubtypeIsSupertype("ConnectedBattery", "Battery"));
            patterns.Add(new SubtypeIsSupertype("ConnectedBattery", "ConnectedDCVoltageSource"));
            patterns.Add(new SubtypeIsSupertype("ConnectedDCPowerSupply", "DCPowerSupply"));
            patterns.Add(new SubtypeIsSupertype("ConnectedDCPowerSupply", "ConnectedDCVoltageSource"));

            patterns.Add(new TypeCannotBeTypeFromSet("ConnectedBattery", new List<string>() { "ConnectedDCPowerSupply" }));
            patterns.Add(new TypeCannotBeTypeFromSet("ConnectedDCPowerSupply", new List<string>() { "ConnectedBattery" }));

            patterns.Add(new TypeMustBeInRelationWithType("ConnectedDCVoltageSource", "ElectricalConnection"));
            patterns.Add(new TypeMustBeInRelationWithType("ElectricalConnection", "ElectricalDevice"));*/

            using (StreamWriter writer = new StreamWriter(output, false))
            {
                foreach (TextPattern pattern in patterns)
                {
                    writer.WriteLine(pattern.GenerateText());
                }
            }

            Console.WriteLine("DONE!");
        }
    }
}