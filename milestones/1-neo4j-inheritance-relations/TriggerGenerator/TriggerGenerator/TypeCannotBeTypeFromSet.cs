﻿namespace TriggerGenerator
{
    internal class TypeCannotBeTypeFromSet : TextPattern
    {
        public string Type { get; set; }

        public List<string> TypeSet { get; set; }

        public TypeCannotBeTypeFromSet(string type, List<string> typeSet)
        {
            Type = type;
            TypeSet = typeSet;
        }

        public override string GenerateText()
        {
            List<string> typeSetUpper = TypeSet.Select(x => x.ToUpper()).ToList();
            List<string> typeSetLower = TypeSet.Select(x => x.ToLower()).ToList();

            List<string> typeSetLabelExists = typeSetUpper.Select(s => $"apoc.label.exists(node, '{s}')").ToList();

            string commaSet = typeSetUpper.Aggregate((acc, s) => $"{acc}, {s}");
            string orSet = TypeSet.Aggregate((acc, s) => $"{acc} or {s}");
            string orLowerSet = typeSetLower.Aggregate((acc, s) => $"{acc}_or_{s}");
            string labelExistsSet = typeSetLabelExists.Aggregate((acc, s) => $"{acc} or {s}");

            return $@"//{Type.ToUpper()}-SET{{{commaSet}}}
CALL apoc.trigger.add(""{Type.ToLower()}_cannot_be_{orLowerSet}"",
""UNWIND apoc.trigger.nodesByLabel($assignedLabels, '{Type.ToUpper()}') AS node
CALL apoc.util.validate({labelExistsSet}, '{Type} instance cannot be {orSet} instance', null)
RETURN null"", {{phase:'before'}});
";
        }
    }
}
