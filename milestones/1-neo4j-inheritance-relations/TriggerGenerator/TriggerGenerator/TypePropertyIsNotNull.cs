﻿namespace TriggerGenerator
{
    internal class TypePropertyIsNotNull : TextPattern
    {
        public string TypeName { get; set; }

        public string PropertyName { get; set; }

        public TypePropertyIsNotNull(string typeName, string propertyName)
        {
            TypeName = typeName;
            PropertyName = propertyName;
        }

        public override string GenerateText() => $@"//{TypeName.ToUpper()}-{PropertyName.ToUpper()}
CREATE CONSTRAINT {TypeName.ToLower()}_{PropertyName.ToLower()}_not_null IF NOT EXISTS FOR (t:{TypeName.ToUpper()}) REQUIRE t.{PropertyName} IS NOT NULL;
";
    }
}
