// ----- CLASSES & ATTRIBUTES
Class ConnectedElectricalDevice = new Class("ConnectedElectricalDevice", ClassStereotype.Role);
Class ElectricalConnection = new Class("ElectricalConnection", ClassStereotype.Relator);
ElectricalConnection.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
ElectricalConnection.Attributes.Add(new ClassAttribute("resistance", DataType.Float));
Class ConnectedDCVoltageSource = new Class("ConnectedDCVoltageSource", ClassStereotype.RoleMixin);
ConnectedDCVoltageSource.Attributes.Add(new ClassAttribute("drawnCurrent", DataType.Float));
Class ConnectedBattery = new Class("ConnectedBattery", ClassStereotype.Role);
Class ConnectedDCPowerSupply = new Class("ConnectedDCPowerSupply", ClassStereotype.Role);

// ----- GENERALIZATIONS
Generalization ElectricalDeviceConnectedElectricalDevice = new Generalization(ElectricalDevice, ConnectedElectricalDevice);
Generalization ConnectedDCVoltageSourceConnectedDCPowerSupply = new Generalization(ConnectedDCVoltageSource, ConnectedDCPowerSupply);
Generalization ConnectedDCVoltageSourceConnectedBattery = new Generalization(ConnectedDCVoltageSource, ConnectedBattery);
Generalization DCPowerSupplyConnectedDCPowerSupply = new Generalization(DCPowerSupply, ConnectedDCPowerSupply);
Generalization BatteryConnectedBattery = new Generalization(Battery, ConnectedBattery);

// ----- GENERALIZATION SETS
GeneralizationSet ConnectedDCVoltageSourceRoleSet = new GeneralizationSet(new() { ConnectedDCVoltageSourceConnectedDCPowerSupply, ConnectedDCVoltageSourceConnectedBattery }, false, true);

// ----- ASSOCIATIONS
BinaryAssociation ElectricalConnectionConnectedElectricalDevice = new BinaryAssociation(ElectricalConnection, ConnectedElectricalDevice, Multiplicity.OneToMany, Multiplicity.OneToMany);
BinaryAssociation ConnectedDCVoltageSourceElectricalConnection = new BinaryAssociation(ConnectedDCVoltageSource, ElectricalConnection, Multiplicity.OneToMany, Multiplicity.OneToMany);
