// ----- CLASSES & ATTRIBUTES
Class ElectricalDevice = new Class("ElectricalDevice", ClassStereotype.Kind);
ElectricalDevice.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
ElectricalDevice.Attributes.Add(new ClassAttribute("nominalCurrent", DataType.Float));
ElectricalDevice.Attributes.Add(new ClassAttribute("nominalVoltage", DataType.Float));

Class DCPowerSupply = new Class("DCPowerSupply", ClassStereotype.Kind);
DCPowerSupply.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
DCPowerSupply.Attributes.Add(new ClassAttribute("inputVoltage", DataType.Float));
DCPowerSupply.Attributes.Add(new ClassAttribute("maximalCurrent", DataType.Float));

Class Battery = new Class("Battery", ClassStereotype.Kind);
Battery.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
Battery.Attributes.Add(new ClassAttribute("maximalVoltage", DataType.Float));
Battery.Attributes.Add(new ClassAttribute("minimalVoltage", DataType.Float));
