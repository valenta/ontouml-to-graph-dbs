// ----- CLASSES & ATTRIBUTES
Class Rechargeable = new Class("Rechargeable", ClassStereotype.Subkind);
Rechargeable.Attributes.Add(new ClassAttribute("cyclelife", DataType.Integer));
Class Nonrechargeable = new Class("Nonrechargeable", ClassStereotype.Subkind);
Nonrechargeable.Attributes.Add(new ClassAttribute("shelflife", DataType.Integer));

// ----- GENERALIZATIONS
Generalization BatteryRechargeable = new Generalization(Battery, Rechargeable);
Generalization BatteryNonrechargeable = new Generalization(Battery, Nonrechargeable);

// ----- GENERALIZATION SETS
GeneralizationSet BatterySubkindSet = new GeneralizationSet(new() { BatteryRechargeable, BatteryNonrechargeable }, true, true);
