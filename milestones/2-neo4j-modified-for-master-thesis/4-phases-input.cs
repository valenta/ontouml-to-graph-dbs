// ----- CLASSES & ATTRIBUTES
Class Charged = new Class("Charged", ClassStereotype.Phase);
Charged.Attributes.Add(new ClassAttribute("chargeDate", DataType.Date));
Class Used = new Class("Used", ClassStereotype.Phase);
Used.Attributes.Add(new ClassAttribute("lastUseDate", DataType.Date));
Class Discharged = new Class("Discharged", ClassStereotype.Phase);
Discharged.Attributes.Add(new ClassAttribute("dischargeDate", DataType.Date));

// ----- GENERALIZATIONS
Generalization BatteryCharged = new Generalization(Battery, Charged);
Generalization BatteryUsed = new Generalization(Battery, Used);
Generalization BatteryDischarged = new Generalization(Battery, Discharged);

// ----- GENERALIZATION SETS
GeneralizationSet BatteryPhaseSet = new GeneralizationSet(new() { BatteryCharged, BatteryUsed, BatteryDischarged }, true, true);
