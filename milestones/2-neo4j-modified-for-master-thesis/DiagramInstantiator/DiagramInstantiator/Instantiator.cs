﻿using DiagramInstantiator.Neo4j;
using DiagramInstantiator.OntoUML;

namespace DiagramInstantiator
{
    public class Instantiator
    {
        public List<Constraint> InstantiateDataTypeOfAttributes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (Class c in diagram.Classes)
                foreach (ClassAttribute a in c.Attributes)
                    constraints.Add(
                        new PropertyMustBeOfDatatype(
                            c.Name,
                            a.Name,
                            a.DataType.Name
                        )
                    );

            return constraints;
        }
        public List<Constraint> InstantiateMandatorityOfAttributes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (Class c in diagram.Classes)
                foreach (ClassAttribute a in c.Attributes)
                    if (a.Multiplicity.LowerBound > 0)
                        constraints.Add(
                            new PropertyMustBePresent(
                                c.Name,
                                a.Name
                            )
                        );

            return constraints;
        }
        public List<Constraint> InstantiateUniquenessOfAttributes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (Class c in diagram.Classes)
                foreach (ClassAttribute a in c.Attributes)
                    if (a.IsID)
                        constraints.Add(
                            new PropertyMustBeUnique(
                                c.Name,
                                a.Name
                            )
                        );

            return constraints;
        }
        public List<Constraint> InstantiateAttributes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            constraints.AddRange(InstantiateDataTypeOfAttributes(diagram));
            constraints.AddRange(InstantiateMandatorityOfAttributes(diagram));
            constraints.AddRange(InstantiateUniquenessOfAttributes(diagram));

            return constraints;
        }

        public List<Constraint> InstantiateAnyIdentityOfStereotypes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (Class c in diagram.Classes)
                if (!c.Stereotype.HasIdentity)
                {
                    List<Class> relatedSortals = diagram.Classes
                        .Where(r => r.Stereotype.HasIdentity)
                        .Where(r => r.Superclasses(diagram.Generalizations)
                            .Any(s => c.Equals(s) || c.Superclasses(diagram.Generalizations).Contains(s)))
                        .ToList();

                    List<Class> intransitivelyRelatedSortals = relatedSortals
                        .Where(i => i.Superclasses(diagram.Generalizations)
                            .All(s => !relatedSortals.Contains(s)))
                        .ToList();
                    
                    constraints.Add(
                        new LabelMustBeInCombination(
                            c.Name,
                            intransitivelyRelatedSortals
                                .Select(i => i.Name)
                                .ToList()
                        )
                    );
                }
                   

            return constraints;
        }
        public List<Constraint> InstantiateSingleIdentityOfStereotypes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (Class c in diagram.Classes)
                if (c.Stereotype.ProvidesIdentity)
                {
                    List<Class> otherIdentityProviders = diagram.Classes
                        .Where(p => p.Stereotype.ProvidesIdentity)
                        .Where(p => !p.Equals(c))
                        .ToList();

                    if (otherIdentityProviders.Count > 0)
                        constraints.Add(
                            new LabelCannotBeInCombination(
                                c.Name,
                                otherIdentityProviders
                                    .Select(p => p.Name)
                                    .ToList()
                            )
                        );
                }

            return constraints;
        }
        public List<Constraint> InstantiateStereotypes(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            constraints.AddRange(InstantiateAnyIdentityOfStereotypes(diagram));
            constraints.AddRange(InstantiateSingleIdentityOfStereotypes(diagram));

            return constraints;
        }

        public List<Constraint> InstantiateGeneralizations(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (Generalization g in diagram.Generalizations)
                constraints.Add(
                    new LabelMustBeInCombination(
                        g.Subclass.Name,
                        new() { g.Superclass.Name }
                    )
                );

            return constraints;
        }

        public List<Constraint> InstantiateIsCoveringGeneralizationSets(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (GeneralizationSet gs in diagram.GeneralizationSets)
                if (gs.IsCovering)
                    constraints.Add(
                        new LabelMustBeInCombination(
                            gs.Superclass().Name,
                            gs.Subclasses()
                                .Select(s => s.Name)
                                .ToList()
                        )
                    );

            return constraints;
        }
        public List<Constraint> InstantiateIsDisjointGeneralizationSets(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (GeneralizationSet gs in diagram.GeneralizationSets)
                if (gs.IsDisjoint)
                    foreach (Class c in gs.Subclasses())
                        constraints.Add(
                            new LabelCannotBeInCombination(
                                c.Name,
                                gs.Subclasses()
                                    .Where(s => !s.Equals(c))
                                    .Select(s => s.Name)
                                    .ToList()
                            )
                        );

            return constraints;
        }
        public List<Constraint> InstantiateGeneralizationSets(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            constraints.AddRange(InstantiateIsCoveringGeneralizationSets(diagram));
            constraints.AddRange(InstantiateIsDisjointGeneralizationSets(diagram));

            return constraints;
        }

        public List<Constraint> InstantiateMandatorityOfAssociations(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            foreach (BinaryAssociation a in diagram.Associations)
            {
                if(a.FirstMultiplicity.LowerBound > 0)
                    constraints.Add(
                        new RelationshipMustBePresent(
                            a.SecondClass.Name,
                            a.FirstClass.Name
                        )
                    );

                if (a.SecondMultiplicity.LowerBound > 0)
                    constraints.Add(
                        new RelationshipMustBePresent(
                            a.FirstClass.Name,
                            a.SecondClass.Name
                        )
                    );
            }

            return constraints;
        }
        public List<Constraint> InstantiateAssociations(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            constraints.AddRange(InstantiateMandatorityOfAssociations(diagram));

            return constraints;
        }

        public List<Constraint> InstantiateDiagram(ClassDiagram diagram)
        {
            List<Constraint> constraints = new List<Constraint>();

            constraints.AddRange(InstantiateAttributes(diagram));
            constraints.AddRange(InstantiateStereotypes(diagram));
            constraints.AddRange(InstantiateGeneralizations(diagram));
            constraints.AddRange(InstantiateGeneralizationSets(diagram));
            constraints.AddRange(InstantiateAssociations(diagram));

            return constraints;
        }
    }
}
