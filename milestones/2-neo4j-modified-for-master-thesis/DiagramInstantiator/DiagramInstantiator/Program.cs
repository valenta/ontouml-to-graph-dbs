﻿using DiagramInstantiator.Neo4j;
using DiagramInstantiator.OntoUML;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DiagramInstantiator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // ----- Object model of OntoUML diagram

            Console.WriteLine("Loading object model...");

            ClassDiagram diagram = CreateDiagramElectricalDevices();

            // ----- Instantiation of OntoUML model and generation of triggers

            Instantiator instantiator = new Instantiator();
            Generator generator = new Generator(instantiator);

            Console.WriteLine("Generating comments...");
            File.WriteAllText("comments.txt", generator.GenerateDiagramCode(diagram, true));

            Console.WriteLine("Generating triggers...");
            File.WriteAllText("triggers.cql", generator.GenerateDiagramCode(diagram, false));

            Console.WriteLine("DONE!");
        }

        static ClassDiagram CreateDiagramAnimals()
        {
            // ----- CLASSES & ATTRIBUTES

            Class animal = new Class("Animal", ClassStereotype.Kind);
            animal.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
            animal.Attributes.Add(new ClassAttribute("name", DataType.String));

            Class lion = new Class("Lion", ClassStereotype.Subkind);
            lion.Attributes.Add(new ClassAttribute("mane_size", DataType.Float));

            Class tiger = new Class("Tiger", ClassStereotype.Subkind);
            tiger.Attributes.Add(new ClassAttribute("lanes_amount", DataType.Integer));

            // ----- GENERALIZATIONS

            Generalization animalLion = new Generalization(animal, lion);
            Generalization animalTiger = new Generalization(animal, tiger);

            // ----- GENERALIZATION SETS

            GeneralizationSet animalLionTiger = new GeneralizationSet(new() { animalLion, animalTiger }, true, true);

            // ----- DIAGRAM

            ClassDiagram diagram = new ClassDiagram()
            {
                Classes = new() { animal, lion, tiger },
                Generalizations = new() { animalLion, animalTiger },
                GeneralizationSets = new() { animalLionTiger }
            };

            return diagram;
        }

        static ClassDiagram CreateDiagramElectricalDevices()
        {
            // ----- CLASSES & ATTRIBUTES
            Class ElectricalDevice = new Class("ElectricalDevice", ClassStereotype.Kind);
            ElectricalDevice.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
            ElectricalDevice.Attributes.Add(new ClassAttribute("nominalCurrent", DataType.Float));
            ElectricalDevice.Attributes.Add(new ClassAttribute("nominalVoltage", DataType.Float));
            Class DCPowerSupply = new Class("DCPowerSupply", ClassStereotype.Kind);
            DCPowerSupply.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
            DCPowerSupply.Attributes.Add(new ClassAttribute("inputVoltage", DataType.Float));
            DCPowerSupply.Attributes.Add(new ClassAttribute("maximalCurrent", DataType.Float));
            Class Battery = new Class("Battery", ClassStereotype.Kind);
            Battery.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
            Battery.Attributes.Add(new ClassAttribute("maximalVoltage", DataType.Float));
            Battery.Attributes.Add(new ClassAttribute("minimalVoltage", DataType.Float));

            Class Rechargeable = new Class("Rechargeable", ClassStereotype.Subkind);
            Rechargeable.Attributes.Add(new ClassAttribute("cyclelife", DataType.Integer));
            Class Nonrechargeable = new Class("Nonrechargeable", ClassStereotype.Subkind);
            Nonrechargeable.Attributes.Add(new ClassAttribute("shelflife", DataType.Integer));

            Class EnergyStorage = new Class("EnergyStorage", ClassStereotype.Category);
            EnergyStorage.Attributes.Add(new ClassAttribute("capacity", DataType.Float));
            Class DCVoltageSource = new Class("DCVoltageSource", ClassStereotype.Category);
            DCVoltageSource.Attributes.Add(new ClassAttribute("actualVoltage", DataType.Float));
            DCVoltageSource.Attributes.Add(new ClassAttribute("nominalVoltage", DataType.Float));

            Class Charged = new Class("Charged", ClassStereotype.Phase);
            Charged.Attributes.Add(new ClassAttribute("chargeDate", DataType.Date));
            Class Used = new Class("Used", ClassStereotype.Phase);
            Used.Attributes.Add(new ClassAttribute("lastUseDate", DataType.Date));
            Class Discharged = new Class("Discharged", ClassStereotype.Phase);
            Discharged.Attributes.Add(new ClassAttribute("dischargeDate", DataType.Date));

            Class ConnectedElectricalDevice = new Class("ConnectedElectricalDevice", ClassStereotype.Role);
            Class ElectricalConnection = new Class("ElectricalConnection", ClassStereotype.Relator);
            ElectricalConnection.Attributes.Add(new ClassAttribute("id", DataType.Integer, true));
            ElectricalConnection.Attributes.Add(new ClassAttribute("resistance", DataType.Float));
            Class ConnectedDCVoltageSource = new Class("ConnectedDCVoltageSource", ClassStereotype.RoleMixin);
            ConnectedDCVoltageSource.Attributes.Add(new ClassAttribute("drawnCurrent", DataType.Float));
            Class ConnectedBattery = new Class("ConnectedBattery", ClassStereotype.Role);
            Class ConnectedDCPowerSupply = new Class("ConnectedDCPowerSupply", ClassStereotype.Role);

            // ----- GENERALIZATIONS
            Generalization BatteryRechargeable = new Generalization(Battery, Rechargeable);
            Generalization BatteryNonrechargeable = new Generalization(Battery, Nonrechargeable);

            Generalization EnergyStorageBattery = new Generalization(EnergyStorage, Battery);
            Generalization DCVoltageSourceDCPowerSupply = new Generalization(DCVoltageSource, DCPowerSupply);
            Generalization DCVoltageSourceBattery = new Generalization(DCVoltageSource, Battery);

            Generalization BatteryCharged = new Generalization(Battery, Charged);
            Generalization BatteryUsed = new Generalization(Battery, Used);
            Generalization BatteryDischarged = new Generalization(Battery, Discharged);

            Generalization ElectricalDeviceConnectedElectricalDevice = new Generalization(ElectricalDevice, ConnectedElectricalDevice);
            Generalization ConnectedDCVoltageSourceConnectedDCPowerSupply = new Generalization(ConnectedDCVoltageSource, ConnectedDCPowerSupply);
            Generalization ConnectedDCVoltageSourceConnectedBattery = new Generalization(ConnectedDCVoltageSource, ConnectedBattery);
            Generalization DCPowerSupplyConnectedDCPowerSupply = new Generalization(DCPowerSupply, ConnectedDCPowerSupply);
            Generalization BatteryConnectedBattery = new Generalization(Battery, ConnectedBattery);

            // ----- GENERALIZATION SETS
            GeneralizationSet BatterySubkindSet = new GeneralizationSet(new() { BatteryRechargeable, BatteryNonrechargeable }, true, true);
            GeneralizationSet BatteryPhaseSet = new GeneralizationSet(new() { BatteryCharged, BatteryUsed, BatteryDischarged }, true, true);
            GeneralizationSet DCVoltageSourceKindSet = new GeneralizationSet(new() { DCVoltageSourceDCPowerSupply, DCVoltageSourceBattery }, false, true);
            GeneralizationSet ConnectedDCVoltageSourceRoleSet = new GeneralizationSet(new() { ConnectedDCVoltageSourceConnectedDCPowerSupply, ConnectedDCVoltageSourceConnectedBattery }, false, true);

            // ----- ASSOCIATIONS

            BinaryAssociation ElectricalConnectionConnectedElectricalDevice = new BinaryAssociation(ElectricalConnection, ConnectedElectricalDevice, Multiplicity.OneToMany, Multiplicity.OneToMany);
            BinaryAssociation ConnectedDCVoltageSourceElectricalConnection = new BinaryAssociation(ConnectedDCVoltageSource, ElectricalConnection, Multiplicity.OneToMany, Multiplicity.OneToMany);

            // ----- DIAGRAM

            ClassDiagram diagram = new ClassDiagram()
            {
                Classes = new() 
                {
                    ElectricalDevice,
                    DCPowerSupply,
                    Battery,
                    Rechargeable,
                    Nonrechargeable,
                    EnergyStorage,
                    DCVoltageSource,
                    Charged,
                    Used,
                    Discharged,
                    ConnectedElectricalDevice,
                    ElectricalConnection,
                    ConnectedDCVoltageSource,
                    ConnectedBattery,
                    ConnectedDCPowerSupply
                },
                Generalizations = new() 
                {
                    BatteryRechargeable,
                    BatteryNonrechargeable,
                    EnergyStorageBattery,
                    DCVoltageSourceDCPowerSupply,
                    DCVoltageSourceBattery,
                    BatteryCharged,
                    BatteryUsed,
                    BatteryDischarged,
                    ElectricalDeviceConnectedElectricalDevice,
                    ConnectedDCVoltageSourceConnectedDCPowerSupply,
                    ConnectedDCVoltageSourceConnectedBattery,
                    DCPowerSupplyConnectedDCPowerSupply,
                    BatteryConnectedBattery
                },
                GeneralizationSets = new() 
                {
                    BatterySubkindSet,
                    BatteryPhaseSet,
                    DCVoltageSourceKindSet,
                    ConnectedDCVoltageSourceRoleSet
                },
                Associations = new()
                {
                    ElectricalConnectionConnectedElectricalDevice,
                    ConnectedDCVoltageSourceElectricalConnection
                }
            };

            return diagram;
        }
    }
}