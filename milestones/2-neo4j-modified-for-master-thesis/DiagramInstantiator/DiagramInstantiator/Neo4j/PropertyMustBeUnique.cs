﻿namespace DiagramInstantiator.Neo4j
{
    public class PropertyMustBeUnique : Constraint
    {
        public string LabelName { get; set; }
        public string PropertyName { get; set; }

        public PropertyMustBeUnique(string labelName, string propertyName)
        {
            LabelName = labelName;
            PropertyName = propertyName;
        }

        public override string ConstraintName() => "PROPERTY_MUST_BE_UNIQUE";
        public override bool IsBuiltInConstraint() => true;

        public override string ToCommentText()
        {
            return $"{ConstraintName()}({LabelName}, {PropertyName})";
        }
        public override List<string> ToConstraintNames()
        {
            return new() { $"{LabelName}_{PropertyName}_must_be_unique" };
        }
        public override List<string> ToQueryCodes()
        {
            return new() { $@"FOR (node:{LabelName}) REQUIRE node.{PropertyName} IS UNIQUE" };
        }
    }
}
