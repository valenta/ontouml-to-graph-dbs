﻿namespace DiagramInstantiator.Neo4j
{
    public class PropertyMustBeOfDatatype : Constraint
    {
        public string LabelName { get; set; }
        public string PropertyName { get; set; }
        public string DataTypeName { get; set; }

        public PropertyMustBeOfDatatype(string labelName, string propertyName, string dataTypeName)
        {
            LabelName = labelName;
            PropertyName = propertyName;
            DataTypeName = dataTypeName;
        }

        public override string ConstraintName() => "PROPERTY_MUST_BE_OF_DATATYPE";
        public override bool IsBuiltInConstraint() => false;

        public override string ToCommentText()
        {
            return $"{ConstraintName()}({LabelName}, {PropertyName}, {DataTypeName})";
        }
        public override List<string> ToConstraintNames()
        {
            return new() { $"{LabelName}_{PropertyName}_must_be_datatype_{DataTypeName}" };
        }
        public override List<string> ToQueryCodes()
        {
            return new() {
$@"UNWIND (
    apoc.trigger.nodesByLabel($assignedLabels, ""{LabelName}"") + 
    apoc.trigger.nodesByLabel($assignedNodeProperties, ""{LabelName}"")
) AS node
CALL apoc.util.validate(
    exists(node.{PropertyName}) and not 
    apoc.meta.cypher.isType(node.{PropertyName}, ""{DataTypeName}""), 
    ""{PropertyName} property of {LabelName} must be of datatype {DataTypeName}"", 
    null
)
RETURN null"
            };
        }
    }
}
