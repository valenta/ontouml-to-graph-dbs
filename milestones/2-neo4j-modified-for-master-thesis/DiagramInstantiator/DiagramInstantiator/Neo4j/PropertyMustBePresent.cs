﻿namespace DiagramInstantiator.Neo4j
{
    public class PropertyMustBePresent : Constraint
    {
        public string LabelName { get; set; }
        public string PropertyName { get; set; }

        public PropertyMustBePresent(string labelName, string propertyName)
        {
            LabelName = labelName;
            PropertyName = propertyName;
        }

        public override string ConstraintName() => "PROPERTY_MUST_BE_PRESENT";
        public override bool IsBuiltInConstraint() => false;

        public override string ToCommentText()
        {
            return $"{ConstraintName()}({LabelName}, {PropertyName})";
        }
        public override List<string> ToConstraintNames()
        {
            return new() { $"{LabelName}_{PropertyName}_must_be_present" };
        }
        public override List<string> ToQueryCodes()
        {
            return new() {
$@"UNWIND (
    $createdNodes + 
    apoc.trigger.nodesByLabel($assignedLabels, ""{LabelName}"") + 
    apoc.trigger.nodesByLabel($removedNodeProperties, ""{LabelName}"")
) AS node
CALL apoc.util.validate(
    apoc.label.exists(node, ""{LabelName}"") and not 
    exists(node.{PropertyName}), 
    ""{PropertyName} property of {LabelName} must be present"", 
    null
)
RETURN null"
            };
        }
    }
}
