﻿namespace DiagramInstantiator.Neo4j
{
    public class LabelMustBeInCombination : Constraint
    {
        public string LabelName { get; set; }
        public List<string> OtherLabelNames { get; set; }

        public LabelMustBeInCombination(string labelName, List<string> otherLabelNames)
        {
            LabelName = labelName;
            OtherLabelNames = otherLabelNames;
        }

        public override string ConstraintName() => "LABEL_MUST_BE_IN_COMBINATION";
        public override bool IsBuiltInConstraint() => false;

        public override string ToCommentText()
        {
            string otherNamesCommaString = OtherLabelNames.Aggregate((acc, el) => $"{acc}, {el}");
            return $"{ConstraintName()}({LabelName}, {{{otherNamesCommaString}}})";
        }
        public override List<string> ToConstraintNames()
        {
            string otherNamesOrUnderscoreString = OtherLabelNames.Aggregate((acc, el) => $"{acc}_or_{el}");
            return new() { $"{LabelName}_must_be_with_{otherNamesOrUnderscoreString}" };
        }
        public override List<string> ToQueryCodes()
        {
            string nodesByLabelString = OtherLabelNames
                .Select(el => $"apoc.trigger.nodesByLabel($removedLabels, \"{el}\")")
                .Aggregate((acc, el) => $"{acc} + {el}");

            string labelExistsString = OtherLabelNames
                .Select(el => $"apoc.label.exists(node, \"{el}\")")
                .Aggregate((acc, el) => $"{acc} or {el}");

            string otherNamesOrString = OtherLabelNames
                .Aggregate((acc, el) => $"{acc} or {el}");

            return new() {
$@"UNWIND (
    apoc.trigger.nodesByLabel($assignedLabels, ""{LabelName}"") + 
    {nodesByLabelString}
) AS node
CALL apoc.util.validate(
    apoc.label.exists(node, ""{LabelName}"") and not 
    ({labelExistsString}), 
    ""{LabelName} label must be in a combination with {otherNamesOrString} labels"", 
    null
)
RETURN null"
            };
        }
    }
}
