﻿namespace DiagramInstantiator.Neo4j
{
    public class LabelCannotBeInCombination : Constraint
    {
        public string LabelName { get; set; }
        public List<string> OtherLabelNames { get; set; }

        public LabelCannotBeInCombination(string labelName, List<string> otherLabelNames)
        {
            LabelName = labelName;
            OtherLabelNames = otherLabelNames;
        }

        public override string ConstraintName() => "LABEL_CANNOT_BE_IN_COMBINATION";
        public override bool IsBuiltInConstraint() => false;

        public override string ToCommentText()
        {
            string otherNamesCommaString = OtherLabelNames.Aggregate((acc, el) => $"{acc}, {el}");
            return $"{ConstraintName()}({LabelName}, {{{otherNamesCommaString}}})";
        }
        public override List<string> ToConstraintNames()
        {
            string otherNamesOrUnderscoreString = OtherLabelNames.Aggregate((acc, el) => $"{acc}_or_{el}");
            return new() { $"{LabelName}_cannot_be_with_{otherNamesOrUnderscoreString}" };
        }
        public override List<string> ToQueryCodes()
        {
            string labelExistsString = OtherLabelNames
                .Select(el => $"apoc.label.exists(node, \"{el}\")")
                .Aggregate((acc, el) => $"{acc} or {el}");

            string otherNamesOrString = OtherLabelNames
                .Aggregate((acc, el) => $"{acc} or {el}");

            return new() {
$@"UNWIND (
    apoc.trigger.nodesByLabel($assignedLabels, ""{LabelName}"")
) AS node
CALL apoc.util.validate(
    {labelExistsString}, 
    ""{LabelName} label cannot be in a combination with {otherNamesOrString} labels"", 
    null
)
RETURN null"
            };
        }
    }
}
