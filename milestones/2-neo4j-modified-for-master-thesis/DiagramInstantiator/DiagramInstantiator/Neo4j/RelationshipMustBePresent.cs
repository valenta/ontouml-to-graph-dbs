﻿namespace DiagramInstantiator.Neo4j
{
    public class RelationshipMustBePresent : Constraint
    {
        public string RelatedLabelName { get; set; }
        public string MandatoryLabelName { get; set; }

        public RelationshipMustBePresent(string relatedLabelName, string mandatoryLabelName)
        {
            RelatedLabelName = relatedLabelName;
            MandatoryLabelName = mandatoryLabelName;
        }

        public override string ConstraintName() => "RELATIONSHIP_MUST_BE_PRESENT";
        public override bool IsBuiltInConstraint() => false;

        public override string ToCommentText()
        {
            return $"{ConstraintName()}({RelatedLabelName}, {MandatoryLabelName})";
        }
        public override List<string> ToConstraintNames()
        {
            return new() { 
                $"{RelatedLabelName}_must_be_in_relationship_with_{MandatoryLabelName}_labelassigned",
                $"{RelatedLabelName}_must_be_in_relationship_with_{MandatoryLabelName}_labelremoved",
                $"{RelatedLabelName}_must_be_in_relationship_with_{MandatoryLabelName}_relationdeleted"
            };
        }
        public override List<string> ToQueryCodes()
        {
            return new() {
$@"UNWIND (
    apoc.trigger.nodesByLabel($assignedLabels, ""{RelatedLabelName}"")
) AS node1
CALL apoc.util.validate(
    SIZE([(node1)--(n2:{MandatoryLabelName}) | n2 ]) < 1, 
    ""{RelatedLabelName} label must be in relationship with {MandatoryLabelName} label"", 
    null
)
RETURN null",
$@"UNWIND (
    apoc.trigger.nodesByLabel($removedLabels, ""{MandatoryLabelName}"")
) AS node2
UNWIND (
    [(n1:{RelatedLabelName})--(node2) | n1 ]
) AS node1
CALL apoc.util.validate(
    SIZE([(node1)--(n2:{MandatoryLabelName}) | n2 ]) < 1, 
    ""{RelatedLabelName} label must be in relationship with {MandatoryLabelName} label"", 
    null
)
RETURN null",
$@"UNWIND (
    $deletedRelationships
) AS rel
UNWIND (
    [apoc.rel.startNode(rel), apoc.rel.endNode(rel)]
) AS node1
CALL apoc.util.validate(
    apoc.label.exists(node1, ""{RelatedLabelName}"") and 
    (SIZE([(node1)--(n2:{MandatoryLabelName}) | n2 ]) < 1), 
    ""{RelatedLabelName} label must be in relationship with {MandatoryLabelName} label"", 
    null
)
RETURN null"
            };
        }
    }
}
