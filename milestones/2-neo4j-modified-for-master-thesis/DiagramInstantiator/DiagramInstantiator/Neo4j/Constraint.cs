﻿using System.Text;

namespace DiagramInstantiator.Neo4j
{
    public abstract class Constraint
    {
        public abstract string ConstraintName();
        public abstract bool IsBuiltInConstraint();

        public abstract string ToCommentText();
        public abstract List<string> ToConstraintNames();
        public abstract List<string> ToQueryCodes();

        public string ToConstraintCode()
        {
            StringBuilder builder = new StringBuilder();

            string commentText = ToCommentText();
            builder.AppendLine($"//{commentText.ToUpper()}");

            List<string> constraintNames = ToConstraintNames();
            List<string> queryCodes = ToQueryCodes();

            for (int i = 0; i < constraintNames.Count; i++)
            {
                string constraintName = constraintNames[i];
                string queryCode = queryCodes[i];
                if (IsBuiltInConstraint())
                    builder.Append(GenerateConstraintCreateCode(constraintName, queryCode));
                else
                    builder.Append(GenerateTriggerAddCode(constraintName, queryCode));
            }

            return builder.ToString();
        }

        protected string GenerateConstraintCreateCode(string constraintName, string queryCode)
        {
            return
$@"CREATE CONSTRAINT {constraintName.ToLower()} IF NOT EXISTS {queryCode};
";
        }
        protected string GenerateTriggerAddCode(string triggerName, string queryCode)
        {
            return
$@"CALL apoc.trigger.add('{triggerName.ToLower()}', 
'{queryCode}', {{ phase:'before' }});
";
        }
    }
}
