﻿using DiagramInstantiator.Neo4j;
using DiagramInstantiator.OntoUML;
using System.Text;

namespace DiagramInstantiator
{
    public class Generator
    {
        public Instantiator Instantiator { get; set; }

        public Generator(Instantiator instantiator)
        {
            Instantiator = instantiator;
        }

        public string GenerateAttributesCode(ClassDiagram diagram, bool onlyComments = false)
        {
            return GenerateCode(
                "ATTRIBUTES",
                Instantiator.InstantiateAttributes(diagram),
                onlyComments
            );
        }
        public string GenerateStereotypesCode(ClassDiagram diagram, bool onlyComments = false)
        {
            return GenerateCode(
                "STEREOTYPES",
                Instantiator.InstantiateStereotypes(diagram),
                onlyComments
            );
        }
        public string GenerateGeneralizationsCode(ClassDiagram diagram, bool onlyComments = false)
        {
            return GenerateCode(
                "GENERALIZATIONS",
                Instantiator.InstantiateGeneralizations(diagram),
                onlyComments
            );
        }
        public string GenerateGeneralizationSetsCode(ClassDiagram diagram, bool onlyComments = false)
        {
            return GenerateCode(
                "GENERALIZATION SETS",
                Instantiator.InstantiateGeneralizationSets(diagram),
                onlyComments
            );
        }
        public string GenerateAssociationsCode(ClassDiagram diagram, bool onlyComments = false)
        {
            return GenerateCode(
                "ASSOCIATIONS",
                Instantiator.InstantiateAssociations(diagram),
                onlyComments
            );
        }

        public string GenerateDiagramCode(ClassDiagram diagram, bool onlyComments = false)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine(GenerateAttributesCode(diagram, onlyComments));
            builder.AppendLine(GenerateStereotypesCode(diagram, onlyComments));
            builder.AppendLine(GenerateGeneralizationsCode(diagram, onlyComments));
            builder.AppendLine(GenerateGeneralizationSetsCode(diagram, onlyComments));
            builder.AppendLine(GenerateAssociationsCode(diagram, onlyComments));

            return builder.ToString();
        }

        protected string GenerateCode(string comment, List<Constraint> constraints, bool onlyComments)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"// ----- {comment}\n");

            foreach (Constraint constraint in constraints)
                if (onlyComments)
                    builder.AppendLine(constraint.ToCommentText());
                else
                    builder.AppendLine(constraint.ToConstraintCode());

            return builder.ToString();
        }
    }
}
