﻿namespace DiagramInstantiator.OntoUML
{
    public class BinaryAssociation
    {
        public Class FirstClass { get; set; }
        public Class SecondClass { get; set; }

        public Multiplicity FirstMultiplicity { get; set; }
        public Multiplicity SecondMultiplicity { get; set; }

        public BinaryAssociation(string firstClassName, string secondClassName)
        {
            FirstClass = new Class(firstClassName);
            SecondClass = new Class(secondClassName);
            FirstMultiplicity = Multiplicity.ZeroToMany;
            SecondMultiplicity = Multiplicity.ZeroToMany;
        }
        public BinaryAssociation(string firstClassName, string secondClassName, Multiplicity firstMultiplicity, Multiplicity secondMultiplicity)
        {
            FirstClass = new Class(firstClassName);
            SecondClass = new Class(secondClassName);
            FirstMultiplicity = firstMultiplicity;
            SecondMultiplicity = secondMultiplicity;
        }
        public BinaryAssociation(Class firstClass, Class secondClass)
        {
            FirstClass = firstClass;
            SecondClass = secondClass;
            FirstMultiplicity = Multiplicity.ZeroToMany;
            SecondMultiplicity = Multiplicity.ZeroToMany;
        }
        public BinaryAssociation(Class firstClass, Class secondClass, Multiplicity firstMultiplicity, Multiplicity secondMultiplicity)
        {
            FirstClass = firstClass;
            SecondClass = secondClass;
            FirstMultiplicity = firstMultiplicity;
            SecondMultiplicity = secondMultiplicity;
        }

        public override string? ToString()
        {
            return $"{FirstClass.Name} [{FirstMultiplicity}] ---- [{SecondMultiplicity}] {SecondClass.Name}";
        }
    }
}
