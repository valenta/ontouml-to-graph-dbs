﻿namespace DiagramInstantiator.OntoUML
{
    public class Generalization
    {
        public Class Superclass { get; set; }
        public Class Subclass { get; set; }

        public Generalization(string superclassName, string subclassName)
        {
            Superclass = new Class(superclassName);
            Subclass = new Class(subclassName);
        }
        public Generalization(Class superclass, Class subclass)
        {
            Superclass = superclass;
            Subclass = subclass;
        }

        public override string? ToString()
        {
            return $"{Superclass.Name} <|---- {Subclass.Name}";
        }
        public override bool Equals(object? obj)
        {
            return obj is Generalization generalization &&
                   Superclass.Equals(generalization.Superclass) &&
                   Subclass.Equals(generalization.Subclass);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Superclass, Subclass);
        }
    }
}
