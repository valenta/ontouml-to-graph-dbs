﻿namespace DiagramInstantiator.OntoUML
{
    public class Multiplicity
    {
        public int LowerBound { get; set; }
        public int UpperBound { get; set; }

        public Multiplicity(int lowerBound, int upperBound)
        {
            if (lowerBound > upperBound)
                throw new ArgumentException("Multiplicity upper bound must be greater than lower bound.");

            LowerBound = lowerBound;
            UpperBound = upperBound;
        }

        public override string? ToString()
        {
            string lowerBoundString = LowerBound.ToString();
            string upperBoundString = UpperBound != int.MaxValue ? UpperBound.ToString() : "*";
            return $"{lowerBoundString}..{upperBoundString}";

        }
        public override bool Equals(object? obj)
        {
            return obj is Multiplicity multiplicity &&
                   LowerBound == multiplicity.LowerBound &&
                   UpperBound == multiplicity.UpperBound;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(LowerBound, UpperBound);
        }

        public static Multiplicity ZeroToOne { get; } = new Multiplicity(0, 1);
        public static Multiplicity ExactlyOne { get; } = new Multiplicity(1, 1);
        public static Multiplicity ZeroToMany { get; } = new Multiplicity(0, int.MaxValue);
        public static Multiplicity OneToMany { get; } = new Multiplicity(1, int.MaxValue);
    }
}
