﻿using System.Text;

namespace DiagramInstantiator.OntoUML
{
    public class Class
    {
        public string Name { get; set; }
        public ClassStereotype Stereotype { get; set; }
        public List<ClassAttribute> Attributes { get; set; }

        public Class(string name)
        {
            Name = name;
            Stereotype = ClassStereotype.None;
            Attributes = new List<ClassAttribute>();
        }
        public Class(string name, ClassStereotype stereotype)
        {
            Name = name;
            Stereotype = stereotype;
            Attributes = new List<ClassAttribute>();
        }
        public Class(string name, ClassStereotype stereotype, List<ClassAttribute> attributes)
        {
            Name = name;
            Stereotype = stereotype;
            Attributes = attributes;
        }

        public override string? ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"{Name} {Stereotype}");

            foreach(ClassAttribute attribute in Attributes)
                builder.AppendLine($"    {attribute}");

            return builder.ToString();
        }
        public override bool Equals(object? obj)
        {
            return obj is Class @class &&
                   Name == @class.Name;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public List<Class> Superclasses(List<Generalization> generalizations)
        {
            List<Class> all = new List<Class>();

            Queue<Class> queue = new Queue<Class>();
            queue.Enqueue(this);

            while(queue.Count > 0)
            {
                Class current = queue.Dequeue();
                List<Class> direct = generalizations
                    .Where(g => g.Subclass.Equals(current))
                    .Select(g => g.Superclass)
                    .ToList();
                foreach(Class super in direct)
                {
                    all.Add(super);
                    queue.Enqueue(super);
                }
            }

            return all;
        }
    }
}
