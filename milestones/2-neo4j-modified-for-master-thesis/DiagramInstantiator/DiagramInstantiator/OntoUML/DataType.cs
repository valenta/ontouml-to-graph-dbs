﻿namespace DiagramInstantiator.OntoUML
{
    public class DataType
    {
        public string Name { get; set; }

        public DataType(string name)
        {
            Name = name;
        }

        public override string? ToString()
        {
            return Name.ToLower();
        }
        public override bool Equals(object? obj)
        {
            return obj is DataType type &&
                   Name == type.Name;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public static DataType Boolean { get; } = new DataType("BOOLEAN");
        public static DataType Integer { get; } = new DataType("INTEGER");
        public static DataType Float { get; } = new DataType("FLOAT");
        public static DataType String { get; } = new DataType("STRING");
        public static DataType Date { get; } = new DataType("DATE");
    }
}
