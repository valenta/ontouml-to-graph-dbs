﻿namespace DiagramInstantiator.OntoUML
{
    public class ClassStereotype
    {
        public string Name { get; set; }
        public bool ProvidesIdentity { get; set; }
        public bool HasIdentity { get; set; }

        public ClassStereotype(string name, bool providesIdentity = false, bool hasIdentity = false)
        {
            if (providesIdentity && !hasIdentity)
                throw new ArgumentException("Class stereotype that provides an identity must also have an identity.");

            Name = name;
            ProvidesIdentity = providesIdentity;
            HasIdentity = hasIdentity;
        }

        public override string? ToString()
        {
            if (!this.Equals(ClassStereotype.None))
                return $"<<{Name}>>";
            else
                return string.Empty;
        }
        public override bool Equals(object? obj)
        {
            return obj is ClassStereotype stereotype &&
                   Name == stereotype.Name;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public static ClassStereotype Kind { get; } = new ClassStereotype("Kind", true, true);
        public static ClassStereotype Relator { get; } = new ClassStereotype("Relator", true, true);
        public static ClassStereotype Mode { get; } = new ClassStereotype("Mode", true, true);
        public static ClassStereotype Quality { get; } = new ClassStereotype("Quality", true, true);
        public static ClassStereotype Subkind { get; } = new ClassStereotype("Subkind", false, true);
        public static ClassStereotype Role { get; } = new ClassStereotype("Role", false, true);
        public static ClassStereotype Phase { get; } = new ClassStereotype("Phase", false, true);
        public static ClassStereotype Category { get; } = new ClassStereotype("Category", false, false);
        public static ClassStereotype Mixin { get; } = new ClassStereotype("Mixin", false, false);
        public static ClassStereotype RoleMixin { get; } = new ClassStereotype("RoleMixin", false, false);
        public static ClassStereotype PhaseMixin { get; } = new ClassStereotype("PhaseMixin", false, false);
        public static ClassStereotype None { get; } = new ClassStereotype("None");
    }
}
