﻿using System.Text;

namespace DiagramInstantiator.OntoUML
{
    public class GeneralizationSet
    {
        public List<Generalization> Generalizations { get; set; }
        public bool IsCovering { get; set; }
        public bool IsDisjoint { get; set; }

        public GeneralizationSet(List<Generalization> generalizations, bool isCovering = false, bool isDisjoint = false)
        {
            if (generalizations.Count < 2)
                throw new ArgumentException("Generalization set must containt at least two generalizations.");

            if (!generalizations.All(g => g.Superclass == generalizations.First().Superclass))
                throw new ArgumentException("All generalizations in generalization set must have same superclass.");

            Generalizations = generalizations;
            IsCovering = isCovering;
            IsDisjoint = isDisjoint;
        }
        public GeneralizationSet(string superclassName, List<string> subclassNames, bool isCovering = false, bool isDisjoint = false)
        {
            if (subclassNames.Count < 2)
                throw new ArgumentException("Generalization set must containt at least two subclasses.");

            Generalizations = new List<Generalization>();

            foreach (string subclassName in subclassNames)
                Generalizations.Add(new Generalization(superclassName, subclassName));

            IsCovering = isCovering;
            IsDisjoint = isDisjoint;
        }
        public GeneralizationSet(Class superclass, List<Class> subclasses, bool isCovering = false, bool isDisjoint = false)
        {
            if (subclasses.Count < 2)
                throw new ArgumentException("Generalization set must containt at least two subclasses.");

            Generalizations = new List<Generalization>();

            foreach (Class subclass in subclasses)
                Generalizations.Add(new Generalization(superclass, subclass));

            IsCovering = isCovering;
            IsDisjoint = isDisjoint;
        }

        public Class Superclass()
        {
            return Generalizations.First().Superclass;
        }
        public List<Class> Subclasses()
        {
            return Generalizations.Select(g => g.Subclass).ToList();
        }

        public string ToPropertyString()
        {
            string complete = IsCovering ? "complete" : "incomplete";
            string disjoint = IsDisjoint ? "disjoint" : "overlapping";
            return $"{{{complete}, {disjoint}}}";
        }

        public override string? ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"{Superclass().Name} {{{ToPropertyString()}}}");

            foreach(Class subclass in Subclasses())
                builder.AppendLine($"    <|---- {subclass.Name}");

            return builder.ToString();
        }
        public override bool Equals(object? obj)
        {
            return obj is GeneralizationSet set &&
                   Superclass().Equals(set.Superclass()) &&
                   Subclasses().All(set.Subclasses().Contains);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Superclass(), Subclasses());
        }
    }
}
