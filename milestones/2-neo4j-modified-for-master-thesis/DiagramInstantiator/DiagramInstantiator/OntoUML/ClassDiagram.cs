﻿namespace DiagramInstantiator.OntoUML
{
    public class ClassDiagram
    {
        public List<Class> Classes { get; set; }
        public List<Generalization> Generalizations { get; set; }
        public List<GeneralizationSet> GeneralizationSets { get; set; }
        public List<BinaryAssociation> Associations { get; set; }

        public ClassDiagram()
        {
            Classes = new List<Class>();
            Generalizations = new List<Generalization>();
            GeneralizationSets = new List<GeneralizationSet>();
            Associations = new List<BinaryAssociation>();
        }
    }
}
