﻿namespace DiagramInstantiator.OntoUML
{
    public class ClassAttribute
    {
        public string Name { get; set; }
        public DataType DataType { get; set; }
        public Multiplicity Multiplicity { get; set; }
        public bool IsID { get; set; }

        public ClassAttribute(string name, DataType dataType)
        {
            Name = name;
            DataType = dataType;
            Multiplicity = Multiplicity.ExactlyOne;
            IsID = false;
        }
        public ClassAttribute(string name, DataType dataType, bool isID)
        {
            Name = name;
            DataType = dataType;
            Multiplicity = Multiplicity.ExactlyOne;
            IsID = isID;
        }
        public ClassAttribute(string name, DataType dataType, Multiplicity multiplicity)
        {
            Name = name;
            DataType = dataType;
            Multiplicity = multiplicity;
            IsID = false;
        }
        public ClassAttribute(string name, DataType dataType, Multiplicity multiplicity, bool isID)
        {
            Name = name;
            DataType = dataType;
            Multiplicity = multiplicity;
            IsID = isID;
        }

        public override string? ToString()
        {
            if(!Multiplicity.Equals(Multiplicity.ExactlyOne))
                return $"{Name}: {DataType} [{Multiplicity}]";
            else
                return $"{Name}: {DataType}";
        }
        public override bool Equals(object? obj)
        {
            return obj is ClassAttribute attribute &&
                   Name == attribute.Name;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }
    }
}
