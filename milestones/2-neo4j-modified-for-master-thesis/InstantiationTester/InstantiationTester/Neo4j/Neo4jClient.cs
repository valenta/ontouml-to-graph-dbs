﻿using Neo4j.Driver;

namespace InstantiationTester.Neo4j
{
    public class Neo4jClient : IDisposable
    {
        private bool _disposed;
        private IDriver _driver;

        public Neo4jClient(Neo4jSettings settings)
        {
            _driver = GraphDatabase.Driver(settings.URI, settings.AuthToken);
        }

        ~Neo4jClient()
        {
            Dispose(disposing: false);
        }

        public void Execute(string query)
        {
            using var session = _driver.Session();
            session.WriteTransaction(tx =>
            {
                var result = tx.Run(query);
                return result;
            });
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _driver.Dispose();
                }

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
