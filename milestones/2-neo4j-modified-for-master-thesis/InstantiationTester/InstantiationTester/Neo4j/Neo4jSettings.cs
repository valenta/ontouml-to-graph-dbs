﻿using Neo4j.Driver;

namespace InstantiationTester.Neo4j
{
    public class Neo4jSettings
    {
        public string URI { get; set; }
        public IAuthToken AuthToken { get; set; }

        public Neo4jSettings(string uri, string username, string password)
        {
            URI = uri;
            AuthToken = AuthTokens.Basic(username, password);
        }

        public static Neo4jSettings CreateDefault()
        {
            return new Neo4jSettings("bolt://localhost:7687", "neo4j", "Abc123456");
        }
    }
}
