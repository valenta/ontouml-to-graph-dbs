using InstantiationTester.Neo4j;

namespace InstantiationTester
{
    public class NegativePhasesTest
    {
        private Neo4jSettings _settings;

        [SetUp]
        public void Setup()
        {
            _settings = Neo4jSettings.CreateDefault();
        }

        [Test]
        public void TestInCombinationGeneralizationOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (c
    :Charged {
        chargeDate: date(""2022-10-28"")
});"
                );
            });
        }

        [Test]
        public void TestInCombinationCompleteOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (b
    :EnergyStorage :DCVoltageSource
    :Battery :Rechargeable {
        id: 10,
        capacity: 3.0,
        actualVoltage: 1.45,
        nominalVoltage: 1.2,
        maximalVoltage: 1.45,
        minimalVoltage: 1.2,
        cyclelife: 1000
});"
                );
            });
        }

        [Test]
        public void TestInCombinationCompleteOnRemove()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (b:Battery {id: 1})
REMOVE b:Charged;"
                );
            });
        }

        [Test]
        public void TestNotCombinationDisjointOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (b
    :EnergyStorage :DCVoltageSource
    :Battery :Rechargeable :Charged :Used {
        id: 10,
        capacity: 3.0,
        actualVoltage: 1.45,
        nominalVoltage: 1.2,
        maximalVoltage: 1.45,
        minimalVoltage: 1.2,
        cyclelife: 1000,
        chargeDate: date(""2022-10-28""),
        lastUseDate: date(""2022-10-26"")
});"
                );
            });
        }

        [Test]
        public void TestNotCombinationDisjointOnUpdate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (b:Battery {id: 1})
SET b:Used
SET b += {lastUseDate: date(""2022-10-26"")};"
                );
            });
        }
    }
}