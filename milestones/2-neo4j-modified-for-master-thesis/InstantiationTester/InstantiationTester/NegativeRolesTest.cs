using InstantiationTester.Neo4j;

namespace InstantiationTester
{
    public class NegativeRolesTest
    {
        private Neo4jSettings _settings;

        [SetUp]
        public void Setup()
        {
            _settings = Neo4jSettings.CreateDefault();
        }

        [Test]
        public void TestInCombinationHasIdentityOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 3})
SET e:ConnectedElectricalDevice
CREATE (c
    :ConnectedDCVoltageSource {
        drawnCurrent: 0.025
})-[:Mediation]->(r
    :ElectricalConnection {id: 10, resistance: 0.05}
)-[:Mediation]->(e);"
                );
            });
        }

        [Test]
        public void TestInCombinationHasIdentityOnRemove()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (b:Battery {id: 1})
REMOVE b:ConnectedBattery;"
                );
            });
        }

        [Test]
        public void TestInCombinationGeneralizationRoleMixinOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (b
    :EnergyStorage :DCVoltageSource
    :Battery :Rechargeable :Charged :ConnectedBattery {
        id: 10,
        capacity: 3.0,
        actualVoltage: 1.45,
        nominalVoltage: 1.2,
        maximalVoltage: 1.45,
        minimalVoltage: 1.2,
        cyclelife: 1000,
        chargeDate: date(""2022-10-28"")
});"
                );
            });
        }

        [Test]
        public void TestInCombinationGeneralizationRoleMixinOnRemove()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (b:Battery {id: 1})
REMOVE b:ConnectedDCVoltageSource;"
                );
            });
        }

        [Test]
        public void TestInCombinationGeneralizationKindOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 3})
SET e:ConnectedElectricalDevice
CREATE (c
    :ConnectedDCVoltageSource 
    :ConnectedBattery {drawnCurrent: 0.025}
)-[:Mediation]->(r
    :ElectricalConnection {id: 10, resistance: 0.05}
)-[:Mediation]->(e);"
                );
            });
        }

        [Test]
        public void TestRelationshipPresentOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (b
    :EnergyStorage :DCVoltageSource
    :Battery :Rechargeable :Charged :ConnectedBattery :ConnectedDCVoltageSource  {
        id: 10,
        capacity: 3.0,
        actualVoltage: 1.45,
        nominalVoltage: 1.2,
        maximalVoltage: 1.45,
        minimalVoltage: 1.2,
        cyclelife: 1000,
        chargeDate: date(""2022-10-28""),
        drawnCurrent: 0.025
})-[:Mediation]->(r
    :ElectricalConnection{id: 10, resistance: 0.05}
);"
                );
            });
        }

        [Test]
        public void TestRelationshipPresentOnRemove()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 1})
REMOVE e:ConnectedElectricalDevice;"
                );
            });
        }

        [Test]
        public void TestRelationshipPresentOnDelete()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 1})
DETACH 
DELETE e;"
                );
            });
        }
    }
}