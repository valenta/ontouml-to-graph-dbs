using InstantiationTester.Neo4j;

namespace InstantiationTester
{
    public class NegativeCategoriesTest
    {
        private Neo4jSettings _settings;

        [SetUp]
        public void Setup()
        {
            _settings = Neo4jSettings.CreateDefault();
        }

        [Test]
        public void TestInCombinationHasIdentityOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (e
    :EnergyStorage {
        capacity: 3.0
});"
                );
            });
        }

        [Test]
        public void TestInCombinationGeneralizationOnCreate()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (b
    :DCVoltageSource
    :Battery :Rechargeable :Charged {
        id: 10,
        actualVoltage: 1.45,
        nominalVoltage: 1.2,
        maximalVoltage: 1.45,
        minimalVoltage: 1.2,
        cyclelife: 1000,
        chargeDate: date(""2022-10-28"")
});"
                );
            });
        }

        [Test]
        public void TestInCombinationGeneralizationOnRemove()
        {
            Assert.Catch(() =>
            {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (b:Battery {id: 1})
REMOVE b:EnergyStorage;"
                );
            });
        }

        [Test]
        public void TestNotCombinationDisjointOnCreate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (b
    :EnergyStorage :DCVoltageSource
    :Battery :DCPowerSupply :Rechargeable :Charged {
        id: 10,
        capacity: 3.0,
        actualVoltage: 1.45,
        nominalVoltage: 1.2,
        maximalVoltage: 1.45,
        minimalVoltage: 1.2,
        cyclelife: 1000,
        chargeDate: date(""2022-10-28""),
        inputVoltage: 230.0,
        maximalCurrent: 1.5
});"
                );
            });
        }
    }
}