using InstantiationTester.Neo4j;

namespace InstantiationTester
{
    public class NegativeKindsTest
    {
        private Neo4jSettings _settings;

        [SetUp]
        public void Setup()
        {
            _settings = Neo4jSettings.CreateDefault();
        }

        [Test]
        public void TestPropertyDatatypeOnCreate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (e
    :ElectricalDevice {
        id: 10,
        nominalVoltage: ""some text"",
        nominalCurrent: 0.025
});"
                );
            });
        }

        [Test]
        public void TestPropertyDatatypeOnUpdate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 1})
SET e.nominalVoltage = ""some text"";"
                );
            });
        }

        [Test]
        public void TestPropertyPresentOnCreate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (e
    :ElectricalDevice {
        id: 10,
        nominalCurrent: 0.025
});"
                );
            });
        }

        [Test]
        public void TestPropertyPresentOnRemove()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 1})
REMOVE e.nominalVoltage;"
                );
            });
        }

        [Test]
        public void TestPropertyUniqueOnCreate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (e
    :ElectricalDevice {
        id: 1,
        nominalVoltage: 1.2,
        nominalCurrent: 0.025
});"
                );
            });
        }

        [Test]
        public void TestPropertyUniqueOnUpdate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"MATCH (e:ElectricalDevice {id: 1}) 
SET e.id = 2;"
                );
            });
        }

        [Test]
        public void TestNotCombinationSingleOnCreate()
        {
            Assert.Catch(() => {
                using Neo4jClient connection = new Neo4jClient(_settings);
                connection.Execute(
@"CREATE (e
    :ElectricalDevice
    :DCVoltageSource :DCPowerSupply {
        id: 10,
        nominalCurrent: 0.025,
        nominalVoltage: 5.0,
        actualVoltage: 4.95,
        inputVoltage: 230.0,
        maximalCurrent: 1.5
});"
                );
            });
        }
    }
}