// ----- CLASSES & ATTRIBUTES
Class EnergyStorage = new Class("EnergyStorage", ClassStereotype.Category);
EnergyStorage.Attributes.Add(new ClassAttribute("capacity", DataType.Float));
Class DCVoltageSource = new Class("DCVoltageSource", ClassStereotype.Category);
DCVoltageSource.Attributes.Add(new ClassAttribute("actualVoltage", DataType.Float));
DCVoltageSource.Attributes.Add(new ClassAttribute("nominalVoltage", DataType.Float));

// ----- GENERALIZATIONS
Generalization EnergyStorageBattery = new Generalization(EnergyStorage, Battery);
Generalization DCVoltageSourceDCPowerSupply = new Generalization(DCVoltageSource, DCPowerSupply);
Generalization DCVoltageSourceBattery = new Generalization(DCVoltageSource, Battery);

// ----- GENERALIZATION SETS
GeneralizationSet DCVoltageSourceKindSet = new GeneralizationSet(new() { DCVoltageSourceDCPowerSupply, DCVoltageSourceBattery }, false, true);
